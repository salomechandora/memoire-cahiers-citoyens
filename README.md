# Memoire Cahiers Citoyens

## Présentation

Ce dépôt contient les codes réalisés dans le cadre du stage de Salomé Chandora au LASTIG sur le corpus «Cahiers Citoyens» (CC). 
Le corpus n'est pas inclus pour des raisons de confidentialité des données (RGPD).

Ces codes pour but d'analyser sémantiquement et spatialement le corpus CC. Plus précisément, il s'agit d'identifier des propositions des contributeurs récurrentes grâce au clustering à partir de plongements de phrases.


## Fichiers

- Pré-traitements: ce dossier contient deux fichiers
	- **clustering-contributions**: le notebook servant à écarter les contributions les plus similaires (pétitions, doublons...) grâce au clustering de contributions 
	- **pretraitements**: le notebook servant à effectuer les pré-traitements sur le corpus
	
- Analyse_corpus: contient les codes ayant servis à analyser les corpus CC et GDN (chapitre 2 du mémoire)
	- ****: le notebook ayant servi à étudier les lemmes les plus fréquents, les entités nommées et les expressions polylexicales des deux corpus
	
